var contactForm = $("form#contactForm");
contactForm.submit(function(event){
	event.preventDefault();

  // Change to your service ID, or keep using the default service
  var service_id = "gmail1994";
  var template_id = "template_mf9k52By";

  contactForm.find("button").text("Enviando...");
  emailjs.sendForm(service_id,template_id,"contactForm")
  	.then(function(){ 
       contactForm.find("button").text("Enviar");
    }, function(err) {
       contactForm.find("button").text("Enviar");
    });
  return false;
});